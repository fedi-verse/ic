<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <link rel="stylesheet" href="/static/pure-css/pure-min.css">
    <link rel="stylesheet" href="/static/pure-css/grids-responsive-min.css">
    <link rel="stylesheet" href="/static/pure-css/forms-min.css">
    <link rel="stylesheet" href="/static/pure-css/buttons-min.css">
    <link rel="stylesheet" href="/static/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/custom.css" >
    
    <title>Fedi health</title>

  </head>

<body>

<div class="pure-g">
    <div class="pure-u-5-5" style="text-align:center;">
    <h1>
        <a href="/trusted-nodes"><i class="fa fa-heart"></i></a>
        <a href="/bans/"><i class="fa fa-microphone-slash"></i></a>
        <a href="/"><i class="fa fa-question-circle-o"></i></a>
        Fedi health
    </h1>

    <div class="pure-form">
    Suggested bans for
    <select onchange="window.location.href='/bans/'+this.value">
        <option value="">All nodes</option>
         %for node in trusted_nodes:
            %if node == excluded_node:
            <option value="{{ node.domain }}" selected>{{ node.domain }}</option>
            %else:
            <option value="{{ node.domain }}">{{ node.domain }}</option>
            %end
        %end
    </select>
    </div>

    </div>
</div>

<div class="pure-g">
    <div class="pure-u-1 pure-u-md-6-24"></div>
    <div class="pure-u-1 pure-u-md-12-24">

    <div style="padding: 1em 0 1em 0;">
        <h2 style="display:inline;">
            <i class="fa fa-microphone-slash"></i> Suggested bans
        </h2>
        <div style="display:inline; float:right">
            %if excluded_node:
            <a class="pure-button button-secondary" style="vertical-align:middle;" href="/raw-bans/{{ excluded_node.domain }}">Raw data</a>
            %else:
            <a class="pure-button button-secondary" style="vertical-align:middle;" href="/raw-bans/">Raw data</a>
            %end
        </div>
    </div>
    
    <table class="pure-table pure-table-bordered" width="100%">
    <thead>
        <th>Node</th>
        <th>Bans</th>
        <th></th>
    </thead>
    <tbody>
    %for domain, ban_cnt in bans:
        <tr>
            <td>{{ domain }}</td>
            <td>{{ ban_cnt }}</td>
            <td><a href="/banned-node/{{ domain }}"><i class="fa fa-eye"></i></a></td>
        </tr>
    %end
    </tbody>
    </table>
    </div>
    <div class="pure-u-1 pure-u-md-6-24"></div>
</div>

</body>
</html>
