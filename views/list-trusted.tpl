<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <link rel="stylesheet" href="/static/pure-css/pure-min.css">
    <link rel="stylesheet" href="/static/pure-css/grids-responsive-min.css">
    <link rel="stylesheet" href="/static/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/custom.css" >
    
    <title>Fedi health</title>

  </head>

<body>

<div class="pure-g">
    <div class="pure-u-5-5" style="text-align:center;">
    <h1>
        <a href="/trusted-nodes"><i class="fa fa-heart"></i></a>
        <a href="/bans/"><i class="fa fa-microphone-slash"></i></a>
        <a href="/"><i class="fa fa-question-circle-o"></i></a>
        Fedi health
    </h1>
    </div>
</div>

<div class="pure-g">
    <div class="pure-u-1 pure-u-md-6-24"></div>
    <div class="pure-u-1 pure-u-md-12-24">
        
    <h2><i class="fa fa-heart"></i> Trusted nodes</h2>
    <table class="pure-table pure-table-bordered" width="100%">
    <thead>
        <th>Node</th>
        <th>Instance</th>
        <th>Bans</th>
        <th></th>
    </thead>
    <tbody>
    %for node in trusted_nodes:
        <tr>
            <td>{{ node.domain }}</td>
            <td>{{ node.instance_of }}</td>
            <td>{{ len(node.banned_nodes) }}</td>
            <td><a href="/trusted-node/{{ node.domain }}"><i class="fa fa-eye"></i></a></td>
        </tr>
    %end
    </tbody>
    </table>
    </div>
    <div class="pure-u-1 pure-u-md-6-24"></div>
</div>

</body>
</html>
