<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <link rel="stylesheet" href="/static/pure-css/pure-min.css">
    <link rel="stylesheet" href="/static/pure-css/grids-responsive-min.css">
    <link rel="stylesheet" href="/static/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/custom.css" >
    
    <title>Fedi health</title>

  </head>

<body>

<div class="pure-g">
    <div class="pure-u-5-5" style="text-align:center;">
    <h1>
        <a href="/trusted-nodes"><i class="fa fa-heart"></i></a>
        <a href="/bans/"><i class="fa fa-microphone-slash"></i></a>
        <a href="/"><i class="fa fa-question-circle-o"></i></a>
        Fedi health
    </h1>
    </div>
</div>

<div class="pure-g">
    <div class="pure-u-1 pure-u-md-6-24"></div>
    
    <div class="pure-u-1 pure-u-md-12-24" style="text-align:center;">
    <h2>
    <img style="vertical-align: middle;" src="/static/images/fediverse_logo.png" />
    Let's keep our network healthy!
    </h2>
    </div>
    
    <div class="pure-u-1 pure-u-md-6-24"></div>
</div>

</body>
</html>
