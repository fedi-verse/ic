"""
“Copyright 2019 Nodos rebeldes”

This file is part of Fediverse policy tracker.

Fediverse policy tracker is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy_json import MutableJson, NestedMutableJson
import json
import sqlite3

Base = declarative_base()
engine = create_engine('sqlite:///fedinet.db')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
db_session = DBSession()

class TrustedNode(Base):
    __tablename__ = 'trusted_node'
    id = Column(Integer, primary_key=True)
    domain = Column(String(250), nullable=False) # FQDN
    instance_of = Column(String(250), nullable=False) # pleroma, gnusocial, mastodont, etc
    federation = Column(MutableJson, nullable=True)
    banned_nodes = Column(NestedMutableJson, default=[])

    def updateBannedNodes(self, banned_nodes):
        """
        The admin of this trusted node may have accidentally included a banned node more than once.
        We only want a list of unique node domains.
        """
        nodes=[]
        for banned_node in banned_nodes:
            if not banned_node in nodes:
                nodes.append(banned_node)
        self.banned_nodes=nodes
        db_session.commit()
