"""
“Copyright 2019 Nodos rebeldes”

This file is part of Fediverse policy tracker.

Fediverse policy tracker is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from bottle import url, route, run, template, response
from bottle import SimpleTemplate, static_file, TEMPLATE_PATH

import os, sys, json
from models import db_session, TrustedNode
from pprint import pprint


BASE_DIR = os.path.dirname(__file__)
print(BASE_DIR)

# Set default attributes to pass into templates.
#SimpleTemplate.defaults = dict(CONF['html'])
#SimpleTemplate.defaults['url'] = url

@route('/static/<filename:path>', name='static')
def serve_static(filename):
    print(os.path.join(BASE_DIR, 'static'))
    print(filename)
    return static_file(filename, root=os.path.join(BASE_DIR, 'static'))

@route('/')
def index():
    return template('index')

@route('/bans/')
@route('/bans/<trusted_domain>')
def bans(trusted_domain=None):
    bans=suggested_bans(trusted_domain)
    excluded = db_session.query(TrustedNode).filter(TrustedNode.domain == trusted_domain).first() if trusted_domain else None
    
    return template('bans', excluded_node = excluded,
                            trusted_nodes = db_session.query(TrustedNode).all(),
                            bans=sorted(bans.items(), key=lambda kv: kv[1], reverse=True))


@route('/raw-bans/')
@route('/raw-bans/<trusted_domain>')
def raw_bans(trusted_domain=None):
    bans=suggested_bans(trusted_domain)
    pprint(bans)
    raw=""
    for domain, cnt in bans.items():
        raw="%s\n%s" % (raw, domain)
    response.content_type = 'text/plain;'
    return raw

@route('/trusted-nodes')
def list_trusted():
    return template('list-trusted', trusted_nodes=db_session.query(TrustedNode).all())

@route('/trusted-node/<domain>')
def trusted_node(domain):
    node=db_session.query(TrustedNode).filter(TrustedNode.domain == domain).first()
    if not node:
        return
    return template('trusted-node', trusted_node=node)

@route('/banned-node/<domain>')
def banned_node(domain):
    nodes = db_session.query(TrustedNode).filter(TrustedNode.banned_nodes.contains(domain)).all()
    return template('banned-node', banned_domain=domain, trusted_nodes=nodes)

def suggested_bans(node_domain):
    if node_domain:
        excluded = db_session.query(TrustedNode).filter(TrustedNode.domain == node_domain).first()
    else:
        excluded = None
    trusted_nodes = db_session.query(TrustedNode).all()
    bans={}
    for trusted in trusted_nodes:
        if trusted == excluded:
            # Do not include the excluded node's bans
            continue
        for banned_node in trusted.banned_nodes:
            if excluded and banned_node in excluded.banned_nodes:
                # We only include bans foriegn to the excluded_node
                continue
            if not banned_node in bans:
                bans[banned_node]=1
            else:
                bans[banned_node]+=1
    return bans

run(host='localhost', port=8080, debug=True, reloader=True)
