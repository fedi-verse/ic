"""
“Copyright 2019 Nodos rebeldes”

This file is part of Fediverse policy tracker.

Fediverse policy tracker is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from six.moves import urllib
import os, sys, json
import requests
from models import *
from pprint import pprint

Base.metadata.bind = engine
Base.metadata.create_all(engine)

TIMEOUT=5

"""
 Database INITIALISATION
 Ensure an entry in database for each domain listed in trusted_nodes.cfg
"""

trusted_nodes=[]
config_filepath = "trusted_nodes.cfg"
if os.path.isfile(config_filepath):
    with open(config_filepath, 'r') as filehandle:
        trusted_nodes = [node.rstrip() for node in filehandle.readlines()]
else:
    sys.exit("Can't find config file %s" % config_filepath)

for trusted_domain in trusted_nodes:
    if not db_session.query(TrustedNode).filter(TrustedNode.domain == trusted_domain).first():
        try:
            res = requests.get('https://' + trusted_domain + '/nodeinfo/2.0.json?', timeout=TIMEOUT)
            if res.json()['software']['name'] == 'pleroma':
                new_node = TrustedNode(domain=trusted_domain, instance_of="pleroma")
                db_session.add(new_node)
                print("Added trusted domain '%s' to database" % trusted_domain)
                continue
        except:
            pass
db_session.commit()

"""
 Update database with federation policies
"""

trusted_nodes = db_session.query(TrustedNode).all()

for trusted_node in trusted_nodes:
    print("Querying: %s" % trusted_node.domain)
    if trusted_node.instance_of == "pleroma":
        res = requests.get('https://' + trusted_node.domain + '/nodeinfo/2.0.json?', timeout=TIMEOUT)
        #pprint(res.json())
        #pprint(res.json()['metadata']['federation'])
        trusted_node.federation=res.json()['metadata']['federation']
        trusted_node.updateBannedNodes(res.json()['metadata']['federation']['mrf_simple']['reject'])
        continue
    
    if trusted_node.instance_of == "mastodon":
        pass

db_session.commit()
